FROM ubuntu:18.04

LABEL maintainer davidmbroin@gmail.com

RUN apt update &&\
    apt install -y \
        build-essential \
        automake autoconf \
        autoconf-archive \
        libtool \
        libboost-program-options-dev \
        libgtkmm-2.4-dev \
        gerbv \
        git \
        zip \
        librsvg2-dev &&\
    git clone https://github.com/pcb2gcode/pcb2gcode.git &&\
    cd pcb2gcode &&\
    git checkout v2.0.0 &&\
    autoreconf -fvi && ./configure && make && make install &&\
    rm -rf /var/lib/apt/list/* 
